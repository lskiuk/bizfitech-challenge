﻿using Bizfitech.Manager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bizfitech.UserManager.InMemoryUsers
{
    public class InMemoryUserRepo : IUserRepo
    {
        private int _id = 1;

        private ICollection<User> _store = new HashSet<User>();

        public InMemoryUserRepo(): this(Enumerable.Empty<User>())
        {
        }

        public InMemoryUserRepo(IEnumerable<User> users)
        {
            foreach (var user in users)
            {
                AddUser(user);
            }
        }

        public Task<int> AddUser(User user)
        {
            if (_store.Any(u => u.AccountNo == user.AccountNo))
            {
                throw new Exception($"Account number {user.AccountNo} is already in use");
            }

            // should also do a check to ensure the bank exists in this system

            var newId = _id++;
            user.Id = newId;
            _store.Add(user);

            return Task.FromResult(newId);
        }

        public Task<User> GetUserById(int id)
        {
            var user = _store.FirstOrDefault(u => u.Id == id);

            return Task.FromResult(user);
        }

        public Task<User> GetUserByNumber(string accountNo)
        {
            var user = _store.FirstOrDefault(u => u.AccountNo == accountNo);

            return Task.FromResult(user);
        }

        public Task<IEnumerable<User>> GetUsers()
        {
            return Task.FromResult(_store.AsEnumerable());
        }
    }
}