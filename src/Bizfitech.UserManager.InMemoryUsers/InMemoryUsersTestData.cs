﻿using Bizfitech.Manager.Models;
using System.Collections.Generic;

namespace Bizfitech.UserManager.InMemoryUsers
{
    public static class InMemoryUsersTestData
    {
        public static IEnumerable<User> Create(string bankA, string bankB) {
            return new[] {
                new User { AccountNo = "12345678", Bank = bankA, FirstName = "Lee", LastName = "Cooper" },
                new User { AccountNo = "87654321", Bank = bankA, FirstName = "Joe", LastName = "Blogs" },
                new User { AccountNo = "23487890", Bank = bankA, FirstName = "Jane", LastName = "Blogs" },
                new User { AccountNo = "34534593", Bank = bankB, FirstName = "Sarah", LastName = "Doe" },
                new User { AccountNo = "32467828", Bank = bankB, FirstName = "David", LastName = "Doe" },
                new User { AccountNo = "92083949", Bank = bankB, FirstName = "Alex", LastName = "Tulley" },
            };
        }
    }
}