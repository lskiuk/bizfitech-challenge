﻿using Bizfitech.Manager.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bizfitech.UserManager
{
    public interface IUserRepo
    {
        Task<int> AddUser(User user);

        Task<User> GetUserById(int id);

        Task<User> GetUserByNumber(string accountNo);

        Task<IEnumerable<User>> GetUsers();
    }
}