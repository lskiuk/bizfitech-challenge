﻿namespace Bizfitech.Manager.Models
{
    public class User
    {
        public int? Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string AccountNo { get; set; }

        public string Bank { get; set; }
    }
}