﻿using Bizfitech.Manager.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bizfitech.UserManager
{
    public class AccountNumberCheck : IUserRepo
    {
        private readonly IUserRepo _userRepo;

        public AccountNumberCheck(IUserRepo userRepo)
        {
            _userRepo = userRepo;
        }

        public async Task<int> AddUser(User user)
        {
            var existingUser = await GetUserByNumber(user.AccountNo);

            if (existingUser != null)
            {
                throw new Exception($"Account number {user.AccountNo} is already in use");
            }

            return await _userRepo.AddUser(user);
        }

        public Task<User> GetUserById(int id) => _userRepo.GetUserById(id);

        public Task<User> GetUserByNumber(string accountNo) => _userRepo.GetUserByNumber(accountNo);

        public Task<IEnumerable<User>> GetUsers() => _userRepo.GetUsers();
    }
}