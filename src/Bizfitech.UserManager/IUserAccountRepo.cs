﻿using BankClient.Models;
using System.Threading.Tasks;

namespace Bizfitech.UserManager
{
    public interface IUserAccountRepo
    {
        Task<Account> GetAccountById(int userId);

        Task<Account> GetAccountByNumber(string accountNo);

        Task<TransactionBreakdown> GetTransactionsById(int userId);

        Task<TransactionBreakdown> GetTransactionsByNumber(string accountNo);
    }
}