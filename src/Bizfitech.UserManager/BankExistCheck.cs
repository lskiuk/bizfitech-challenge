﻿using BankClient;
using Bizfitech.Manager.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bizfitech.UserManager
{
    public class BankExistCheck : IUserRepo
    {
        private readonly IUserRepo _repo;
        private readonly IBankRepoFactory _repoFactory;

        public BankExistCheck(IUserRepo repo, IBankRepoFactory repoFactory)
        {
            _repo = repo;
            _repoFactory = repoFactory;
        }

        public Task<int> AddUser(User user)
        {
            if (!_repoFactory.Exists(user.Bank)) {
                throw new Exception($"The Bank {user.Bank} for user {user.FirstName} {user.LastName} ({user.AccountNo}) does not exist");
            }

            return _repo.AddUser(user);
        }

        public Task<User> GetUserById(int id) => _repo.GetUserById(id);

        public Task<User> GetUserByNumber(string accountNo) => _repo.GetUserByNumber(accountNo);

        public Task<IEnumerable<User>> GetUsers() => _repo.GetUsers();
    }
}