﻿using BankClient;
using BankClient.Models;
using Bizfitech.Manager.Models;
using System;
using System.Threading.Tasks;

namespace Bizfitech.UserManager
{
    public class UserAccountRepo : IUserAccountRepo
    {
        private readonly IUserRepo _userRepo;
        private readonly IBankRepoFactory _factory;

        public UserAccountRepo(IUserRepo userRepo, IBankRepoFactory factory)
        {
            _userRepo = userRepo;
            _factory = factory;
        }

        public async Task<Account> GetAccountById(int userId)
        {
            return await _GetAccount(await _userRepo.GetUserById(userId));
        }

        public async Task<Account> GetAccountByNumber(string accountNo)
        {
            return await _GetAccount(await _userRepo.GetUserByNumber(accountNo));
        }

        public async Task<TransactionBreakdown> GetTransactionsById(int userId)
        {
            return await _GetTransactions(await _userRepo.GetUserById(userId));
        }

        public async Task<TransactionBreakdown> GetTransactionsByNumber(string accountNo)
        {
            return await _GetTransactions(await _userRepo.GetUserByNumber(accountNo));
        }

        private async Task<Account> _GetAccount(User user)
        {
            if (user == null)
            {
                return null;
            }

            var repo = _factory.Resolve(user.Bank);

            return await repo.GetAccountByNumber(user.AccountNo);
        }

        private async Task<TransactionBreakdown> _GetTransactions(User user)
        {
            if (user == null)
            {
                return null;
            }

            var repo = _factory.Resolve(user.Bank);

            return await repo.GetTransactionsByNumber(user.AccountNo);
        }
    }
}