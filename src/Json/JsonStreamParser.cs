﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace Json
{
    public static class JsonStreamParser
    {
        public static T ParseAsJson<T>(this Stream stream)
        {
            if (stream == null) {
                throw new ArgumentNullException(nameof(stream));
            }

            var serializer = JsonSerializer.Create();

            using (var sr = new StreamReader(stream))
            {
                using (var reader = new JsonTextReader(sr))
                {

                    while (reader.Read())
                    {
                        // deserialize only when there's "{" character in the stream
                        if (reader.TokenType == JsonToken.StartObject || reader.TokenType == JsonToken.StartArray)
                        {
                            return serializer.Deserialize<T>(reader);
                        }
                    }
                }
            }

            return default(T);
        }
    }
}
