﻿using System;

namespace BankClient.FairWayBank.Models
{
    internal class FairWayAccountIdentifier
    {
        public string AccountNumber { get; set; }

        public string SortCode { get; set; }
    }
}