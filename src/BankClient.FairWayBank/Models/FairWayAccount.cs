﻿using System;

namespace BankClient.FairWayBank.Models
{
    internal class FairWayAccount
    {
        public string Name { get; set; }

        public FairWayAccountIdentifier Identifier { get; set; }
    }
}