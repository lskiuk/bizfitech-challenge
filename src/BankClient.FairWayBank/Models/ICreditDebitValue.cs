﻿namespace BankClient.FairWayBank.Models
{
    internal interface ICreditDebitValue
    {
        string Type { get; set; }

        double Amount { get; set; }
    }
}