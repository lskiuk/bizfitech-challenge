﻿using System;

namespace BankClient.FairWayBank.Models
{
    internal class FairWayBalance : ICreditDebitValue
    {
        public double Amount { get; set; }

        public string Type { get; set; }

        public FairWayOverdraft Overdraft { get; set; }

        public DateTime DateTime { get; set; }
    }
}