﻿using System;

namespace BankClient.FairWayBank.Models
{
    internal class FairWayTransaction : ICreditDebitValue
    {
        public double Amount { get; set; }

        public string TransactionInformation { get; set; }

        public string Type { get; set; }

        public DateTime BookedDate { get; set; }
    }
}