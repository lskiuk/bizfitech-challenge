﻿using System;

namespace BankClient.FairWayBank.Models
{
    internal class FairWayOverdraft
    {
        public double Amount { get; set; }
    }
}