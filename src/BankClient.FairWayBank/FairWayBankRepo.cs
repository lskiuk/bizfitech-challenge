﻿using BankClient.FairWayBank.Mappings;
using BankClient.FairWayBank.Models;
using BankClient.Models;
using Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BankClient.FairWayBank
{
    public class FairWayBankRepo : IFairWayBankRepo
    {
        private readonly HttpClient _client;
        private readonly string _bankName;

        public FairWayBankRepo(HttpClient client, string bankName)
        {
            if (string.IsNullOrEmpty(bankName))
            {
                throw new ArgumentException("A name of the bank is required");
            }

            _client = client;
            _bankName = bankName;
        }

        public async Task<Account> GetAccountByNumber(string accountNo)
        {
            var tasks = await Task.WhenAll(_GetAccountByNumber(accountNo), _GetOverdraftByNumber(accountNo));

            if (!tasks[0].IsSuccessStatusCode || !tasks[1].IsSuccessStatusCode)
            {
                // In this system 400 is likely to mean not found
                if (tasks[0].StatusCode == System.Net.HttpStatusCode.NotFound || tasks[0].StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return null;
                }

                // Log import error stuff
                throw new Exception($"StatusCode: {tasks[0].StatusCode} Error: {await tasks[0].Content.ReadAsStringAsync()}");
            }

            var account = await tasks[0].ToModel<FairWayAccount>();
            var balance = await tasks[1].ToModel<FairWayBalance>();

            return account.ToAccount(balance, _bankName);
        }

        public async Task<TransactionBreakdown> GetTransactionsByNumber(string accountNo, DateTime? from = null)
        {
            var request = await _client.GetAsync($"/api/v1/accounts/{accountNo}/transactions");

            if (!request.IsSuccessStatusCode)
            {
                if (request.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return null;
                }

                // Log stuff
                throw new Exception($"StatusCode: {request.StatusCode} Error: {await request.Content.ReadAsStringAsync()}");
            }

            var transactions = await request.ToModel<List<FairWayTransaction>>();

            return new TransactionBreakdown
            {
                AccountNo = accountNo,
                Transations = transactions.Select(t => t.ToTransaction())
            };
        }

        private async Task<HttpResponseMessage> _GetAccountByNumber(string accountNo) {
            return await _client.GetAsync($"/api/v1/accounts/{accountNo}");
        }

        private async Task<HttpResponseMessage> _GetOverdraftByNumber(string accountNo) {
            return await _client.GetAsync($"/api/v1/accounts/{accountNo}/balance");
        }
    }

    public static class HttpResponseMessageExt
    {
        public static async Task<T> ToModel<T>(this HttpResponseMessage response)
        {
            using (var stream = await response.Content.ReadAsStreamAsync())
            {
                return stream.ParseAsJson<T>();
            }
        }
    }
}