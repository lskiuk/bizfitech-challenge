﻿using BankClient.FairWayBank.Models;
using BankClient.Models;
using System;

namespace BankClient.FairWayBank.Mappings
{
    public static class FairWayBankModelsExt
    {
        internal static Account ToAccount(this FairWayAccount account, FairWayBalance balance, string bankName)
        {
            if (account == null)
            {
                throw new ArgumentNullException(nameof(account));
            }

            if (account.Identifier == null)
            {
                throw new ArgumentNullException($"To convert a {nameof(FairWayAccount)} to a normalised {nameof(Account)} the Identifier can not be null");
            }

            if (balance == null)
            {
                throw new ArgumentNullException(nameof(balance));
            }

            return new Account
            {
                AccountNo = account.Identifier.AccountNumber,
                SortCode = account.Identifier.SortCode,
                Name = account.Name,
                Balance = balance.TrueAmount(),
                Overdraft = balance.Overdraft?.Amount ?? 0d,
                Bank = bankName
            };
        }

        internal static Transaction ToTransaction(this FairWayTransaction transaction)
        {
            if (transaction == null)
            {
                throw new ArgumentNullException(nameof(transaction));
            }

            return new Transaction
            {
                Amount = transaction.TrueAmount(),
                Date = transaction.BookedDate,
                Information = transaction.TransactionInformation
            };
        }

        private static bool IsDebit(this string str) => "Debit".Equals(str, StringComparison.OrdinalIgnoreCase);

        private static double TrueAmount(this ICreditDebitValue value) => value.Type.IsDebit() ? (value.Amount * -1) : value.Amount;
    }
}