﻿using System;
using System.Collections.Generic;

namespace BankClient
{
    public class BankRepoFactory : IBankRepoFactory
    {
        private Dictionary<string, Func<IBankRepo>> _registered = new Dictionary<string, Func<IBankRepo>>();

        public IBankRepo Resolve(string name)
        {
            if (_registered.TryGetValue(name, out var value))
            {
                return value();
            }

            throw new ArgumentException($"The bank {name} was not reigstered");
        }

        public BankRepoFactory Register(string name, Func<IBankRepo> provider)
        {
            _registered.Add(name, provider);
            return this;
        }

        public bool Exists(string bank)
        {
            return _registered.ContainsKey(bank);
        }

        public void Dispose()
        {
            _registered = null;
        }
    }
}