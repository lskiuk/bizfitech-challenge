﻿using BankClient.Models;
using System;
using System.Threading.Tasks;

namespace BankClient
{
    public interface IBankRepo
    {
        Task<Account> GetAccountByNumber(string accountNo);

        Task<TransactionBreakdown> GetTransactionsByNumber(string accountNo, DateTime? from = null);
    }
}