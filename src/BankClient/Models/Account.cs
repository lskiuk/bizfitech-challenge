﻿using System;

namespace BankClient.Models
{
    public class Account
    {
        public string AccountNo { get; set; }

        public string Name { get; set; }

        public string SortCode { get; set; }

        public double Balance { get; set; }

        public double Overdraft { get; set; }

        public string Bank { get; set; }
    }
}