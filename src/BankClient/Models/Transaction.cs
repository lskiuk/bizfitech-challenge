﻿using System;

namespace BankClient.Models
{
    public class Transaction
    {
        public double Amount { get; set; }

        public string Information { get; set; }

        public DateTime Date { get; set; }
    }
}