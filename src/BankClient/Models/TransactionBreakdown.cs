﻿using System;
using System.Collections.Generic;

namespace BankClient.Models
{
    public class TransactionBreakdown
    {
        public string AccountNo { get; set; }

        public IEnumerable<Transaction> Transations { get; set; }
    }
}