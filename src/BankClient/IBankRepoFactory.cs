﻿using System;

namespace BankClient
{
    public interface IBankRepoFactory : IDisposable
    {
        BankRepoFactory Register(string name, Func<IBankRepo> provider);

        IBankRepo Resolve(string name);

        bool Exists(string bank);
    }
}