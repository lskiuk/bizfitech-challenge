﻿using FluentAssertions;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace BankClient.BizfiBank.Test
{
    public class GetAccountTests
    {
        private BizfiBankRepo _service;
        private readonly ITestOutputHelper _output;

        public GetAccountTests(ITestOutputHelper output)
        {
            _output = output;
            var client = new HttpClient();

            client.BaseAddress = new Uri(Constants.REPO_URL);

            _service = new BizfiBankRepo(client, Constants.BANK_NAME);
        }

        [Fact]
        public async Task GetAccount_WithValidNumber()
        {
            var account = await _service.GetAccountByNumber("74587393");

            account.Should().NotBeNull();
            account.AccountNo.Should().Be("74587393");

            _output.WriteLine($"Account {account.SortCode}-{account.AccountNo} returned with a balance of {account.Balance}");
        }

        [Fact]
        public async Task GetAccount_NotFound()
        {
            var account = await _service.GetAccountByNumber("09876543");

            account.Should().BeNull();
        }

        [Fact]
        public async Task GetAccount_WithInValidNumber()
        {
            Func<Task> func = async () => {
                var account = await _service.GetAccountByNumber("7393");
            };

            func.Should().Throw<Exception>();
        }
    }
}