﻿namespace BankClient.BizfiBank.Test
{
    public static class Constants
    {
        public const string BANK_NAME = "BizfiBank";

        public const string REPO_URL = "http://bizfibank-bizfitech.azurewebsites.net";
    }
}
