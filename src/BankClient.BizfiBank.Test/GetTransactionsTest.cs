﻿using FluentAssertions;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace BankClient.BizfiBank.Test
{
    public class GetTransactionsTest
    {
        private BizfiBankRepo _service;
        private readonly ITestOutputHelper _output;

        public GetTransactionsTest(ITestOutputHelper output)
        {
            _output = output;
            var client = new HttpClient();

            client.BaseAddress = new Uri(Constants.REPO_URL);

            _service = new BizfiBankRepo(client, Constants.BANK_NAME);
        }

        [Fact]
        public async Task GetTransaction_WithValidAccountNum()
        {
            var transactions = await _service.GetTransactionsByNumber("74587393");

            transactions.Should().NotBeNull();
            transactions.AccountNo.Should().Be("74587393");
            transactions.Transations.Should().NotBeEmpty();

            _output.WriteLine($"Transactions for {transactions.AccountNo} returned with {transactions.Transations.Count()} transactions");
        }

        [Fact]
        public async Task GetTransaction_NotFound()
        {
            var transactions = await _service.GetTransactionsByNumber("09876543");

            transactions.Should().BeNull();
        }

        [Fact]
        public async Task GetTransaction_WithInValidNumber()
        {
            Func<Task> func = async () => {
                var transactions = await _service.GetTransactionsByNumber("7393");
            };

            func.Should().Throw<Exception>();
        }
    }
}