﻿using BankClient.FairWayBank.Mappings;
using BankClient.FairWayBank.Models;
using BankClient.Models;
using FluentAssertions;
using System;
using Xunit;

namespace BankClient.FairWayBank.Test
{
    public class TypeConversionTests
    {
        [Fact]
        public void ConvertsCorrectly_PositiveBalance()
        {
            var today = DateTime.Today;

            var fwbAccount = new FairWayAccount
            {
                Name = "Current Account",
                Identifier = new FairWayAccountIdentifier
                {
                    AccountNumber = "12345678",
                    SortCode = "010203"
                }
            };

            var balance = new FairWayBalance
            {
                Amount = 100,
                Type = "Credit",
                DateTime = today,
                Overdraft = new FairWayOverdraft
                {
                    Amount = 50
                }
            };

            var account = fwbAccount.ToAccount(balance, Constants.BANK_NAME);

            account.Should().NotBeNull();
            account.Should().BeEquivalentTo(new Account
            {
                Name = "Current Account",
                AccountNo = "12345678",
                SortCode = "010203",
                Balance = 100,
                Overdraft = 50,
                Bank = Constants.BANK_NAME
            });
        }

        [Fact]
        public void ConvertsCorrectly_NegitiveBalance()
        {
            var today = DateTime.Today;

            var fwbAccount = new FairWayAccount
            {
                Name = "Current Account",
                Identifier = new FairWayAccountIdentifier
                {
                    AccountNumber = "12345678",
                    SortCode = "010203"
                }
            };

            var balance = new FairWayBalance
            {
                Amount = 100,
                Type = "Debit",
                DateTime = today,
                Overdraft = new FairWayOverdraft
                {
                    Amount = 50
                }
            };

            var account = fwbAccount.ToAccount(balance, Constants.BANK_NAME);

            account.Should().NotBeNull();
            account.Should().BeEquivalentTo(new Account
            {
                Name = "Current Account",
                AccountNo = "12345678",
                SortCode = "010203",
                Balance = -100,
                Overdraft = 50,
                Bank = Constants.BANK_NAME
            });
        }
    }
}