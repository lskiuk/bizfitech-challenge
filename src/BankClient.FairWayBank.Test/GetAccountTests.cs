﻿using FluentAssertions;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace BankClient.FairWayBank.Test
{
    public class GetAccountTests
    {
        private IBankRepo _service;
        private readonly ITestOutputHelper _output;

        public GetAccountTests(ITestOutputHelper output)
        {
            _output = output;
            var client = new HttpClient();

            client.BaseAddress = new Uri(Constants.REPO_URL);

            _service = new FairWayBankRepo(client, Constants.BANK_NAME);
        }

        [Fact]
        public async Task GetAccount_ValidNumber_WithoutOverdraft()
        {
            var account = await _service.GetAccountByNumber("74587393");

            account.Should().NotBeNull();
            account.AccountNo.Should().Be("74587393");
            account.Overdraft.Should().Be(0);

            _output.WriteLine($"Account {account.SortCode}-{account.AccountNo} returned with a balance of {account.Balance} and overdraft of {account.Overdraft}");
        }

        [Fact]
        public async Task GetAccount_ValidNumber_WithOverdraft()
        {
            var accountNum = "12345678";
            var account = await _service.GetAccountByNumber(accountNum);

            account.Should().NotBeNull();
            account.AccountNo.Should().Be(accountNum);
            account.Overdraft.Should().NotBe(0);

            _output.WriteLine($"Account {account.SortCode}-{account.AccountNo} returned with a balance of {account.Balance} and overdraft of {account.Overdraft}");
        }



        [Fact]
        public async Task GetAccount_NotFound()
        {
            var account = await _service.GetAccountByNumber("09876543");

            account.Should().BeNull();
        }

        [Fact]
        public async Task GetAccount_InValidNumber()
        {
            Func<Task> func = async () => {
                try
                {
                    var transactions = await _service.GetAccountByNumber("7393");
                }
                catch (Exception ex)
                {
                    _output.WriteLine($"Caught Error: {ex.Message}");
                    throw;
                }
            };

            func.Should().Throw<Exception>();
        }
    }
}