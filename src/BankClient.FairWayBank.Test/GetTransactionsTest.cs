﻿using FluentAssertions;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace BankClient.FairWayBank.Test
{
    public class GetTransactionsTest
    {
        private IBankRepo _service;
        private readonly ITestOutputHelper _output;

        public GetTransactionsTest(ITestOutputHelper output)
        {
            _output = output;
            var client = new HttpClient();

            client.BaseAddress = new Uri(Constants.REPO_URL);

            _service = new FairWayBankRepo(client, Constants.BANK_NAME);
        }

        [Fact]
        public async Task GetTransaction_ValidNumber()
        {
            var transactions = await _service.GetTransactionsByNumber("74587393");

            transactions.Should().NotBeNull();
            transactions.AccountNo.Should().Be("74587393");
            transactions.Transations.Should().NotBeEmpty();

            var transaction = transactions.Transations.ElementAt(0);

            transaction.Information.Should().NotBeNullOrEmpty();
            transaction.Amount.Should().NotBe(0); // This is risky if not having control over the test service

            _output.WriteLine($"Transactions for {transactions.AccountNo} returned with {transactions.Transations.Count()} transactions");
        }

        [Fact]
        public async Task GetTransaction_NotFound()
        {
            var transactions = await _service.GetTransactionsByNumber("09876543");

            transactions.Should().BeNull();
        }

        [Fact]
        public async Task GetTransaction_InValidNumber()
        {
            Func<Task> func = async () => {
                var transactions = await _service.GetTransactionsByNumber("7393");
            };

            func.Should().Throw<Exception>();
        }
    }
}