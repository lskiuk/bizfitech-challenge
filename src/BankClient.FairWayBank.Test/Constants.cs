﻿namespace BankClient.FairWayBank.Test
{
    public class Constants
    {
        public const string BANK_NAME = "FairWayBank";

        public const string REPO_URL = "http://fairwaybank-bizfitech.azurewebsites.net";
    }
}