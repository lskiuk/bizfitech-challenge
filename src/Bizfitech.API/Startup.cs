﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Bizfitech
{
    public class Startup
    {
        public Startup(IConfiguration config, IHostingEnvironment env)
        {
            Environment = env;
            Config = config;
        }

        public IHostingEnvironment Environment { get; }
        public IConfiguration Config { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc(Environment)
                .AddBankFactories(Config)
                .AddUserRepo()
                .AddUserAccountRepo();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            app.UseMvc();
        }
    }
}