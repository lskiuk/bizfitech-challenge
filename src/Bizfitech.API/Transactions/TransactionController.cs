﻿using BankClient.Models;
using Bizfitech.UserManager;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Bizfitech.API.Transactions
{
    [Route("api/transaction")]
    public class TransactionController : Controller
    {
        private readonly IUserAccountRepo _userAccountRepo;

        public TransactionController(IUserAccountRepo userRepo)
        {
            _userAccountRepo = userRepo;
        }

        [HttpGet]
        [Route(@"{accountNo:regex(^\d{{8}}$)}")]
        public async Task<ActionResult<TransactionBreakdown>> GetTransactionsByNumber(string accountNo)
        {
            var breakdown = await _userAccountRepo.GetTransactionsByNumber(accountNo);

            if (breakdown == null)
            {
                return NotFound();
            }

            return Ok(breakdown);
        }
    }
}