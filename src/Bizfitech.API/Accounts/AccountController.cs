﻿using BankClient.Models;
using Bizfitech.UserManager;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Bizfitech.API.Accounts
{
    [Route("api/account")]
    public class AccountController : Controller
    {
        private readonly IUserAccountRepo _userAccountRepo;

        public AccountController(IUserAccountRepo userRepo)
        {
            _userAccountRepo = userRepo;
        }

        [HttpGet]
        [Route(@"{accountNo:regex(^\d{{8}}$)}")]
        public async Task<ActionResult<Account>> GetAccountByNumber(string accountNo)
        {
            var account = await _userAccountRepo.GetAccountByNumber(accountNo);

            if (account == null)
            {
                return NotFound();
            }

            return Ok(account);
        }
    }
}