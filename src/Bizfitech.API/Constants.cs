﻿namespace Bizfitech.API
{
    public static class Constants
    {
        public const string FAIRWAY_BANK_NAME = "FairWayBank";

        public const string BIZFIBANK_BANK_NAME = "BizfiBank";
    }
}