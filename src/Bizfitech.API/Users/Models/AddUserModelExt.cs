﻿using Bizfitech.Manager.Models;

namespace Bizfitech.API.Users.Models
{
    public static class AddUserModelExt
    {
        public static User ToUser(this AddUserModel model)
        {
            return new User
            {
                AccountNo = model.AccountNo,
                Bank = model.Bank,
                FirstName = model.FirstName ?? "",
                LastName = model.LastName
            };
        }
    }
}