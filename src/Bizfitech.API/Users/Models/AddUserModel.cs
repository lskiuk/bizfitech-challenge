﻿using System.ComponentModel.DataAnnotations;

namespace Bizfitech.API.Users.Models
{
    public class AddUserModel
    {
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Account Number is required")]
        [RegularExpression(@"^[\d]{8}$", ErrorMessage = "The Account Number was not in the correct format")]
        public string AccountNo { get; set; }

        [Required(ErrorMessage = "A users Bank is required")]
        public string Bank { get; set; }
    }
}