﻿using Bizfitech.API.Users.Models;
using Bizfitech.Manager.Models;
using Bizfitech.UserManager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bizfitech.API.Users
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepo _userRepo;

        public UserController(IUserRepo userRepo)
        {
            _userRepo = userRepo;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            return Ok(await _userRepo.GetUsers());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<User>>> GetUserById(int id)
        {
            var user = await _userRepo.GetUserById(id);

            if (user == null) {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpPost]
        public async Task<ActionResult<int>> AddUser([FromBody]AddUserModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
            }

            try
            {
                var user = model.ToUser();
                var newId = await _userRepo.AddUser(user);

                return CreatedAtAction(nameof(GetUserById), new { id = newId }, user);
            }
            catch (Exception e)
            {
                // Log error here
                return BadRequest(e.Message);
            }
        }
    }
}