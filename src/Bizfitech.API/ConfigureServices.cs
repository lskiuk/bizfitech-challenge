﻿using BankClient;
using BankClient.BizfiBank;
using BankClient.FairWayBank;
using Bizfitech.UserManager;
using Bizfitech.UserManager.InMemoryUsers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using static Bizfitech.API.Constants;

namespace Bizfitech
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddMvc(this IServiceCollection services, IHostingEnvironment env)
        {
            services.AddMvc(options => {
                // If not dev secure all endpoints
                if (!env.IsDevelopment())
                {
                    options.Filters.Add(new AuthorizeFilter(new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build()));
                }
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            return services;
        }

        public static IServiceCollection AddUserRepo(this IServiceCollection services)
        {
            services.AddSingleton<IUserRepo>(c =>
                new BankExistCheck(
                    new AccountNumberCheck(
                        new InMemoryUserRepo(
                            InMemoryUsersTestData.Create(FAIRWAY_BANK_NAME, BIZFIBANK_BANK_NAME)
                        )
                    ),
                    c.GetService<IBankRepoFactory>()
                 )
            );

            return services;
        }

        public static IServiceCollection AddUserAccountRepo(this IServiceCollection services)
        {
            return services.AddTransient<IUserAccountRepo>(c => new UserAccountRepo(c.GetService<IUserRepo>(), c.GetService<IBankRepoFactory>()));
        }

        public static IServiceCollection AddBankFactories(this IServiceCollection services, IConfiguration config)
        {
            // Registered separately in DI to allow it to dispose of them

            // FairWayBank
            services.AddHttpClient(FAIRWAY_BANK_NAME, client => client.BaseAddress = new Uri(config[$"BankUrls:{FAIRWAY_BANK_NAME}"]));
            services.AddTransient<IFairWayBankRepo>(c => new FairWayBankRepo(c.GetService<IHttpClientFactory>().CreateClient(FAIRWAY_BANK_NAME), FAIRWAY_BANK_NAME));

            // BizfiBank
            services.AddHttpClient(BIZFIBANK_BANK_NAME, client => client.BaseAddress = new Uri(config[$"BankUrls:{BIZFIBANK_BANK_NAME}"]));
            services.AddTransient<IBizfiBankRepo>(c => new BizfiBankRepo(c.GetService<IHttpClientFactory>().CreateClient(BIZFIBANK_BANK_NAME), BIZFIBANK_BANK_NAME));

            services.AddSingleton<IBankRepoFactory>(c =>
                new BankRepoFactory()
                    .Register(FAIRWAY_BANK_NAME, () => c.GetService<IFairWayBankRepo>())
                    .Register(BIZFIBANK_BANK_NAME, () => c.GetService<IBizfiBankRepo>())
            );

            return services;
        }
    }
}