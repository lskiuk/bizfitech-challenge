﻿using BankClient.BizfiBank.Models;
using BankClient.Models;
using System;

namespace BankClient.BizfiBank.Mappings
{
    internal static class BizfiBankModelsExt
    {
        internal static Account ToAccount(this BizfiBankAccount account, string bankName)
        {
            return new Account
            {
                AccountNo = account.Account_Number,
                Name = account.Account_Name,
                Balance = account.Balance,
                Overdraft = account.Overdraft,
                SortCode = account.Sort_Code,
                Bank = bankName
            };
        }

        internal static Transaction ToTransaction(this BizfiBankTransaction account)
        {
            return new Transaction
            {
                Amount = account.Amount,
                Information = account.Merchant,
                Date = account.Cleared_Date
            };
        }
    }
}