﻿using BankClient.BizfiBank.Mappings;
using BankClient.BizfiBank.Models;
using BankClient.Models;
using Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BankClient.BizfiBank
{
    public class BizfiBankRepo : IBizfiBankRepo
    {
        private readonly HttpClient _client;
        private readonly string _bankName;

        public BizfiBankRepo(HttpClient client, string bankName)
        {
            if (string.IsNullOrEmpty(bankName))
            {
                throw new ArgumentException("A name of the bank is required");
            }

            _client = client;
            _bankName = bankName;
        }

        public async Task<Account> GetAccountByNumber(string accountNo)
        {
            var request = await _client.GetAsync($"/api/v1/accounts/{accountNo}");

            if (!request.IsSuccessStatusCode)
            {
                if (request.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return null;
                }

                // Log stuff
                throw new Exception($"StatusCode: {request.StatusCode} Error: {await request.Content.ReadAsStringAsync()}");
            }

            using (var stream = await request.Content.ReadAsStreamAsync())
            {
                var account = stream.ParseAsJson<Models.BizfiBankAccount>();

                return account.ToAccount(_bankName);
            }
        }

        public async Task<TransactionBreakdown> GetTransactionsByNumber(string accountNo, DateTime? from = null)
        {
            var request = await _client.GetAsync($"/api/v1/accounts/{accountNo}/transactions");

            if (!request.IsSuccessStatusCode)
            {
                if (request.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return null;
                }

                // Log stuff
                throw new Exception($"StatusCode: {request.StatusCode} Error: {await request.Content.ReadAsStringAsync()}");
            }

            using (var stream = await request.Content.ReadAsStreamAsync())
            {
                var transactions = stream.ParseAsJson<List<BizfiBankTransaction>>();

                return new TransactionBreakdown
                {
                    AccountNo = accountNo,
                    Transations = transactions.Select(t => t.ToTransaction())
                };
            }
        }
    }
}