﻿using System;

namespace BankClient.BizfiBank.Models
{
    internal class BizfiBankTransaction
    {
        public double Amount { get; set; }

        public string Merchant { get; set; }

        public DateTime Cleared_Date { get; set; }
    }
}