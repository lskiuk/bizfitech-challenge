﻿using System;

namespace BankClient.BizfiBank.Models
{
    internal class BizfiBankAccount
    {
        public string Account_Name { get; set; }

        public string Account_Number { get; set; }

        public string Sort_Code { get; set; }

        public double Balance { get; set; }

        public double Available_Balance { get; set; }

        public double Overdraft { get; set; }
    }
}