# Notes

## Requirements

The application runs either Linux (tested in Ubuntu 16.04) or Windows. The only requirement is the dotnet SDK:

- dotnet core SDK version 2.1.300-rc1-008673

## To Run

Run the following command from the root of the application:

```bash
dotnet run -p ./src/Bizfitech.API
```

_**NB** If you want to run the test runners from the command line you should run `dotnet build ./src` first to ensure the xunit test runners are installed, otherwise its not necessary_

#### Endpoints

- http://localhost:5000/api/user - GET
- http://localhost:5000/api/user - POST
- http://localhost:5000/api/user/{id} - GET
- http://localhost:5000/api/account/{accountNo} - GET
- http://localhost:5000/api/transaction/{accountNo} - GET

_**NB** There is some test data, a list of users with their account numbers and user IDs can be found via **/api/user** (GET) enpoint_


## Assumptions

- Transaction Date

	In this example I have considered 'cleared_date' and 'bookedDate' the same, but in the real world I would confirm that was correct.

- Overdraft

	The over draft amount is purely an indication of the size of possible overdraft amount, not something that effects the balance returned.

- Debit/Credit

	I decided to normalize on positive/negative numbers, That an account in Credit has a positive balance and vice versa.

## Error

The Swagger documentation for FairWayBank could be out of date, when testing I noticed looking at the 400/404 from the FairWayBank get account endpoints they are the same as for BizfiBank, however the swagger "documentation"  as it seems to suggest it only returns 400's regardless. So the basically the wrong attributes are on the endpoint.

## Notes on the Application

- Auth

	I have added code to secure the endpoints, but I have not added authentication, both due to time but also from doing a couple of SSO systems in the past its really easy to make it insecure and I would not want to put in 'poor' authentication.

- HTTPS 

	I have turned off the requirement for HTTPS to ensure running the code is simple your end. Normally I would not go into production with HTTPS off, but there is a bit of a shift in that IIS (nginx) are used as reverse proxies and handle the HTTPS connections.

- Logging

	I havent added much logging, but would generally use the ILoggerFactory, probably with Serilog, to log errors throughout. Also to use LogVerbose() to help with development where needed.

- Throwing Exceptions

	In this project when an 'unexpected' outcome happens I have thrown a generic Exception. Normally I would not do that, if using Exceptions I would subclass an Exception so it can be tested against.

	However after doing investigation into F# and Railroad style programming, I have a different approach these days. Normally if doing an operation where there a 'known' failure paths I would return a Result object that either contained the successful result, or a minimal amount of error information. I would then ensure that at point of failure I would log the detailed error where the most amount of error information is needed.

	This gives several advantages, it means extension methods can be used with the Result object to pipe the success or failure to ensure the railroad programming style mentioned above. I tend to use my Lski.Fn library as a starting point.

- Testing 

	I have not tested as much as I would like, but I have included a couple of test projects for testing against the bank test endpoints so you could see how I would test. Normally I would also try adding a TestServer project to do integration testing to ensure test coverage.
	
- Usage of ToXXX() Functions

	Ive used functions such as ToAccount(this BizfiBankAccount) instead of AutoMapper, partially it is easier to implement when the structures arent flat maps. But also AuotMapper can be a little slow.

	I am a big fan of extensions methods as thier pure nature makes them testable, but also makes it possible to program in a fluent style, which makes it easier to see at a glance whats going on (as long as the naming isnt stupid... naming things can be hard). 
	
- Project Structure

	I have split the structure into multiple projects for a few reasons, it enforces separation of concerns preventing the cross cutting that makes it difficult to pull things apart at a later date, when it is either obviously not needed or in fact broken. Splitting makes it easier to find things as well.
	
	I think it also means you can be sure you are only including 'packages' that are needed, so you can worry less about upgrade paths. This can be seen with the fairly abstract BankDetails project that has no concept of the different implementations and their different needs. 
	
	Also extracting a useful project others into a nuget package to be used on other solutions becomes a simple task, rather than the great spaghetti fight.

	- Organised By 'Feature'

		The controllers/models are not organised by type, they are organised by feature to enable easier removal of code and greater separation where possible. Also makes it easier to pull something out into its own micro service at a later date.

- Use of ViewModels

	Due to time, the most the main Domain models are simply passed out of the API, in this basic system that is acceptable, normally I would create ViewModels to ensure stability of the API. 
	
	One place I have used them is accepting a type of AddUserModel rather than just User, to show in the real world its better to split them. This is for a couple of reasons:

	- It separates concerns of the model requirements, e.g. Account number being required. This is more applicable to the view model as it means the ErrorMessage can be more specific to the view.
	- It means the Domain object is not trying to do 'too much' meaning that if end points are removed, we can safely remove the view models and the logic they required. This reduces the situation of 'redundant code swell' where people are too afraid of deleting code as they dont know the side effects.
	- It means we can be explicit in the contracts between functions, e.g. The AddUser endpoint doesnt require an ID, so dont allow one to be accepted in the first place. This results in less accidental security bugs.

- Caching

	As was mentioned, there is rate limiting to the banks APIs, I havent been able to address this but I would probably implement caching in the same way I have added the functionality to check User account numbers, using a composable class that wraps a IBankRepo that can cache and invalidate values as they are requested.

	I like this composition style as it means you can swap in an out the type of manipulation easily, as if it was a pipeline (like IAppBuilder). You can also be explicit to where you add it and also you can test easily as the logic is separated.

- Open API (Swagger)

	Note that I havent added the attributes for Swagger or Swagger itself. I didnt have time, but Swagger in 2.1 does a better job of understanding whats to be returned and has less requirement on things like the [Produces] attribute.

- Bank Name

	The challenge seemed to talk about bank name as a way of identifying a customers bank API, so the names are fed through most levels, however I would probably have made the IBankRepo.Resolve method expect sort codes and account numbers and use those to pattern match for the correct bank. There might be limitation to that I am not aware of.

